import os

import gradio as gr
import numpy as np
import torch
import torchaudio
import tritonclient.http as httpclient
from whisper.audio import log_mel_spectrogram


def get_log_mel(audiofile):
    max_samples = 30 * 16_000
    samples, sample_rate = torchaudio.load(audiofile)
    if sample_rate != 16_000:
        samples = torchaudio.transforms.Resample(
            orig_freq=sample_rate, new_freq=16_000
        )(samples)
    if samples.shape[1] < max_samples:
        silence = torch.zeros(max_samples - samples.shape[1]).unsqueeze(dim=0)
    samples = torch.concat([samples, silence], dim=1)
    samples_mel = log_mel_spectrogram(samples)
    return samples_mel


def process_audio(audio):
    x_mel = get_log_mel(audio)
    with httpclient.InferenceServerClient(f"{os.getenv('URL_SERVER_TRION')}:8000") as triton_client:
            assert triton_client.is_model_ready(
                model_name="ensemble_whisper_toxicity"
            ), "model ensemble_whisper_toxicity yet ready"
            query = httpclient.InferInput(name="x_mel", shape=x_mel.shape, datatype="FP32")
            model_score = httpclient.InferRequestedOutput(name="prob", binary_data=False)
            query.set_data_from_numpy(np.asarray(x_mel))
            response = triton_client.infer(
                model_name="ensemble_whisper_toxicity", inputs=[query], outputs=[model_score]
            )
            scores = response.as_numpy("prob")
    id2label = {0: "non-toxic", 1: "toxic"}
    res = {"label": id2label[scores.argmax().item()], "score": scores.max().item()}
    return f"These audio is {res['label']} with probability {round(res['score'], 3)}"


if __name__ == "__main__":
    demo = gr.Interface(
        fn=process_audio,
        inputs=[gr.Audio(sources=["upload", "microphone"],
                        type="filepath", label="Input Audio")],
        outputs=["text"]
    )
    demo.launch(show_error=True) # share=True
