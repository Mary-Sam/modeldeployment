Схематично модель для развертывания представлена на рисунке ниже.

![image info](docs/images/scheme.png)

На вход подается аудио, которое после предобработки поступает на вход модели Whisper. Whisper отвечает за преобразования аудио в текст. Далее текст также проходит этап предобработки и подается на вход Bert-based модели. Данная модель - это бинарный классификатор токсичности. После этапа постпроценга на выходе имеется вероятность токсичности текста. Данный ансамбль работает только с английским текстом.

[[_TOC_]]

# Струткура репозитория

```
├── README.md                           <- Главное README.md проекта.
├── requirements.txt
├── .gitlab-ci.yml
├── triton/
│   ├── client/
│   │   ├── client_toxicity.py
│   │   └── client_whisper.py
│   ├── model_repository/
│   │   └── {model_name}/
│   │       ├── config.pbtxt
│   │       └── {version}/
│   │           └── {model_files}
│   └── README.md 
├── docker/
│   ├──server/
│   │  ├──Dockerfile
│   │  └──requirements.txt
│   ├──client/
│   │  ├──app.py
│   │  ├──Dockerfile
│   │  └──requirements.txt
│   └──docker-compose-template.yml
├── chart/
│   ├──templates/
│   ├──Chart.yaml
│   ├──dashboard.json
│   └──values.yaml
├── docs/
│   └──images/
├── onnx/
├── utils/
```

# Triton Inference Server with Gradio

В данном разделе описывается использование NVIDIA Triton для деплоя ML модели. Развертывание осуществляется 2 способами:

- docker-compose
- k8s (minikube)

Triton Inference Server используется совместно с Gradio, который позволяет получить приятный для пользователя интерфейс взаимодействия с моделью.

## Docker-Compose

## Minikube (with Prometheus and Grafana)

В данном проекте в качестве k8s используется minikube. Поскольку модели поднимаются на cuda, то minikube необходимо запускать с NVIDIA GPU. Инструкцию по запуску можно найти по [ссылке](https://minikube.sigs.k8s.io/docs/tutorials/nvidia/).

Также необходимо активировать дополнительный модуль для nvidia-gpu:

```
minikube addons enable nvidia-gpu-device-plugin
```

После чего можно проверить доступность gpu-ресурсов.

```
kubectl get nodes -o=custom-columns=NAME:.metadata.name,GPUs:.status.capacity.'nvidia\.com/gpu' 
```

Установка Grafana с Prometheus осуществляется через чарт `prometheus-community/kube-prometheus-stack`.

```
helm install example-metrics --set prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues=false prometheus-community/kube-prometheus-stack
```

Для получения доступа из браузера к Grafana необходимо выполнить следующую команду:

```
kubectl port-forward service/example-metrics-grafana 8080:80
```

Теперь Grafana будет доступна по пути `http://localhost:8080`. Для входа используется логин - admin с паролем - prom-operator.

Деплой приложения в k8s осуществляется через helm. Для публикации приложения используется сервис типа NodePort.

Чтобы задеплоить в minikube локальный чарт необходимо выполнить команду:

```
cd charts && helm install {deploy_name} .
```

После публикации можно узнать url, по которому доступно приложение.

```
minikube service {service_name} --url
```