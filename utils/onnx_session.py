import numpy as np
import onnxruntime
from onnxruntime import InferenceSession, SessionOptions


def create_onnx_session(
    model_path: str, provider: str = "CPUExecutionProvider"
) -> InferenceSession:
    """Создание сессии для инференса модели с помощью ONNX Runtime.

    @param model_path: путь к модели в формате ONNX
    @param provider: инференс на ЦП
    @return: ONNX Runtime-сессия
    """
    options = SessionOptions()
    options.graph_optimization_level = onnxruntime.GraphOptimizationLevel.ORT_ENABLE_ALL
    options.intra_op_num_threads = 1
    session = InferenceSession(model_path, options, providers=[provider])
    session.disable_fallback()
    return session
