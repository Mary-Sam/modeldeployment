import numpy as np
import torch
from transformers import AutoTokenizer


def preprocessor_toxicity(
    model_id: str, tokenizer: AutoTokenizer, text: str
) -> np.array:
    tokenizer = AutoTokenizer.from_pretrained(model_id)
    emb = list(
        map(
            np.array,
            tokenizer(
                text, truncation=True, padding=True, return_tensors="pt"
            ).values(),
        )
    )
    return emb


def postprocessor_toxicity(pred):
    proba = (torch.sigmoid(torch.Tensor(pred)).cpu().numpy())[0]
    return 1 - proba.T[0] * (1 - proba.T[-1])
