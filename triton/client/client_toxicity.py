import argparse

import numpy as np
import tritonclient.http as httpclient

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t", "--text", type=str, default=None, help="Text for toxicity prediction."
    )
    parser.add_argument(
        "-m",
        "--model_name",
        type=str,
        default="ensemble_toxicity",
        help="Name of model serving in Triton.",
    )
    batch_size = 1
    parser.add_argument(
        "-u", "--url", type=str, default="localhost", help="Triton server url."
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Port=8000 for HTTP, Port=8001 for GRPC",
    )
    args = parser.parse_args()
    url = f"{args.url}:{args.port}"
    triton_client = httpclient.InferenceServerClient(url=url)
    assert triton_client.is_model_ready(
        model_name=args.model_name
    ), f"model {args.model_name} not yet ready"
    model_metadata = triton_client.get_model_metadata(model_name=args.model_name)
    model_config = triton_client.get_model_config(model_name=args.model_name)
    # print(model_metadata)
    # print(model_config)
    query = httpclient.InferInput(name="text", shape=(1, 1), datatype="BYTES")
    model_score = httpclient.InferRequestedOutput(name="prob", binary_data=False)
    query.set_data_from_numpy(np.asarray([[args.text]], dtype=object))
    response = triton_client.infer(
        model_name=args.model_name, inputs=[query], outputs=[model_score]
    )
    scores = response.as_numpy("prob")
    print(scores)
    id2label = {0: "non-toxic", 1: "toxic"}
    res = {"label": id2label[scores.argmax().item()], "score": scores.max().item()}
    print(f"Text: <{args.text}> is {res['label']} with probability {res['score']}")

