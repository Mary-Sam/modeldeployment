import argparse

import numpy as np
import tritonclient.http as httpclient

from utils.processing_data import get_log_mel


def greet(name, intensity):
    return "Hello " * intensity + name + "!"

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # parser.add_argument(
    #     "-a",
    #     "--audio_path",
    #     type=str,
    #     default="data/en_Alex_English_0_24000.wav",
    #     help="Audiopath for transcription prediction.",
    # )
    # parser.add_argument(
    #     "-m",
    #     "--model_name",
    #     type=str,
    #     default="ensemble_whisper",
    #     help="Name of model serving in Triton.",
    # )
    # batch_size = 1
    parser.add_argument(
        "-u", "--url", type=str, default="localhost", help="Triton server url."
    )
    parser.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Port=8000 for HTTP, Port=8001 for GRPC",
    )
    args = parser.parse_args()
    url = f"{args.url}:{args.port}"
    triton_client = httpclient.InferenceServerClient("localhost:8000")
    assert triton_client.is_model_ready(
            model_name=args.model_name
        ), f"model {args.model_name} not yet ready"
    model_metadata = triton_client.get_model_metadata(model_name=args.model_name)
    model_config = triton_client.get_model_config(model_name=args.model_name)
    x_mel = get_log_mel(args.audio_path)
    query = httpclient.InferInput(name="x_mel", shape=x_mel.shape, datatype="FP32")
    model_score = httpclient.InferRequestedOutput(name="text", binary_data=False)
    query.set_data_from_numpy(np.asarray(x_mel))
    response = triton_client.infer(
        model_name=args.model_name, inputs=[query], outputs=[model_score]
    )
    print(response.as_numpy("text"))



