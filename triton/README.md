# Triton Service

## Структура директории

triton/
├── Dockerfile                      <- Докерфайл для поднятия контейнера для Triton сервера
└── model_repository/
    └── {model_name}/               <- Каждая модель имеет отдельную директорию
        ├── config.pbtxt            <- Файл, содержащий детали конфигурации модели
        └── {version}/
            └── {model_files}

### Конфигурация модели
Для каждой модели составляется свой конфигурационный файл `config.pbtxt`

Путь до конфигурационного файла должен выглядеть следующим образом:

```triton/model_repository/{model_name}/config.pbtxt```

Минимальная конфигурация модели должна включать платформу (platform) и/или бэкенд (backend), максимальный размер батча (max_batch_size), а также входные и выходные тензоры модели.

Пример конфигурационного файла:
```
platform: "tensorrt_plan"
max_batch_size: 8
input [
{
    name: "input0"
    data_type: TYPE_FP32
    dims: [ 16 ]
    reshape: { shape: [ ] }
    is_shape_tensor: true
},
{
    name: "input1"
    data_type: TYPE_FP32
    dims: [ 16 ]
}
]
output [
{
    name: "output0"
    data_type: TYPE_FP32
    dims: [ 16 ]
}
]
version_policy: { all { }}
instance_group [
{
    count: 1
    kind: KIND_GPU
    gpus: [ 0, 1, 2 ]
    rate_limiter {
    resources [
        {
        name: "R1"
        count: 4
        },
        {
        name: "R2"
        global: True
        count: 2
        }
    ]
    priority: 2
    }
}
]
```

1. `name` - указывать имя модели необязательно. Если имя модели не будет указано в конфигурации, то подразумевается, что оно совпадает с каталогом хранилища моделей, содержащим модель. Если же имя указано, то оно **должно** совпадать с именем каталога хранилища моделей, содержащего модель;
2. `max_batch_size` - максимальный размер батча при пакетной обработке;
3. `input`, `output` - для каждой входной и выходной переменной должны быть указаны имя (`name`), тип данных (`data_type`) и размер (`dims`). Имя, указанное для входного или выходного тензора, должно соответствовать имени, ожидаемому моделью. Размер тензора задается следующим образом: 
    `dims` задается комбинацией max_batch_size и размером входных/выходных тензоров. Так, если max_batch_size > 0, то полный размер формируется в виде [-1, ] + dims (actual input/output tensors). Если же max_batch_size=0, то размерность [-1, ] в начало не добавляется. Для моделей, поддерживающих входные и выходные тензоры переменного размера, то измерение перменной длины может быть указано как -1.

    Типы данных (`data_type`) входных и выходных тензоров, поддерживаемых в Triton, можно найти по ссылке [Конфигурация модели](https://github.com/triton-inference-server/server/blob/main/docs/user_guide/model_configuration.md) 

    Дополнительные параметры для `input`, `output`:
    1. `reshape` - например, принятый вход от API может отличаться от требуемого входа модели. Также можно использовать для изменения выходного тензора модели.
    2. `is_shape_tensor`

4. `version_policy` - Каждая модель может иметь одну или несколько версий, `version_policy` используется для установки одной из следующих политик:
    - { all: {}} - все версии модели, доступные в репозитории моделей, доступны для инференса;
    - { latest: { num_versions: 2}} - для инференса доступны только последние ‘n’ версий модели в репозитории, последние версии модели имеют наибольшее числовое значение;
    - { specific: { versions: [1,3]}}: для инференса доступны только конкретно перечисленные версии модели.
    
    *Если версия политики не задана, то последняя (с N=1) используется как значение по умолчанию (только последняя версия модели будет доступно в Triton).

5. `instance_group` - по умолчанию для каждого доступного в системе графического процессора создается один экземпляр модели. Параметр `instance-group` можно использовать для развертывания нескольких экземпляров модели на графическом процессоре или только на определенных графических процессорах. Более подробный пример использования `instance_group` [здесь](https://github.com/triton-inference-server/tutorials/tree/main/Conceptual_Guide/Part_2-improving_resource_utilization#concurrent-model-execution). Также можно задать ограничитель ресурсов `rate_limiter`. Более подробно про ограничение ресурсов читаем [здесь](https://github.com/triton-inference-server/server/blob/main/docs/user_guide/model_configuration.md)

Triton также позволяет строить ансамбли моделей. `instance_group` в этом случае прописывается для каждой модели в отдельности.

####Возможные типы моделей

`Sheduler and batching` настраиваются ждя каждой модели в отдельности:

    1. **Stateless Models** - не поддерживает состояния между запросами (пример: CNN - детекция объектов, классификация изображений). Для таких моделей можно использовать `default scheduler or dynamic batcher`. RNN и подобные модели, которые имеют "внутреннюю память", могут быть Stateless Models до тех пор, пока состояние не охватывает ряд запросов инференса. Для таких моделей можно использовать планировщик по умолчанию и нельзя динамический батч.

    2. **Stateful Models** - поддержания сохранения состояния между разными запросами. Для этих моделей необходимо использовать`sequence batcher`, который гарантирует, что все запросы в последовательности будут отправлены на нужный экземпляр модели. Более подробно можно почитать [здесь](https://github.com/triton-inference-server/server/blob/main/docs/user_guide/architecture.md#stateful-models).

    3. **Ensemble Models** - это пайплайн, состоящий из одной и более моделей, где выход одной модели является входом другой. Пример ансамбля: `предварительная обработка данных -> вывод -> постобработка данных`. Использование ансамблевых моделей должно помочь избежать накладных расходов на передаче промежуточных тензоров и свести к минимуму количество запросов, которые будут отправлены в Triton. Пример конфигурации ансамбля моделей можно посмотреть [здесь](https://github.com/triton-inference-server/server/blob/main/docs/user_guide/architecture.md#stateful-models).



### Версия модели

Каждая папка модели может иметь несколько подпапок версий, где каждая версия обозначается числом. Например, следующая структура соответсвует модели с именем "my_model" и одной версией:

```triton/model_repository/my_model/1/```

В папке версий хранятся фактические файлы модели. Эти файлы могут быть в любом формате, поддерживаемом Triton (например, ONNX, TorchScript, Tensorflow, TensorRT и т.д.)

## Запуск Triton сервера

```	
    docker run --gpus=1 --rm -p 8000:8000 -p 8001:8001 -p 8002:8002  \
	-v /home/maria_s/modeldeployment/triton/model_repository:/models nvcr.io/nvidia/tritonserver:23.11-py3 \
	tritonserver --model-repository=/models --strict-model-config=false
```

`--strict-model-config=false` в приведенной выше команде означает, что конфигурация модели (файл `config.pbtxt`) будет автоматически сгенерирована. Данный файл конфигурации также можно настроить самостоятельно.

Для получения сгенерированного автоматический файла конфигурации для конкретной модели также можно обратиться к запущенному серверу

```
curl localhost:8000/v2/models/<your model name>/config | jq >> config.pbtxt
```

При этом Triton генерирует только минимально-необходимую для работы часть конфигурации модели. Необязательные части конфигурации модели необходимо самостоятельно добавить, отредактировав файл config.pbtxt.
   
# Triton Performance Analyzer

Это инструмент командной строки, помогающий оптимизировать производительсность инференса моделей, запущенных на Triton.

```
docker run -it --net=host -v ${PWD}:/workspace/ nvcr.io/nvidia/tritonserver:23.11-py3-sdk bash
```

Внутри запущенного контейнера для анализа производительности модели выполнить команду:

```
# perf_analyzer -m <model name> -b <batch size> --shape <input layer>:<input shape> --concurrency-range <lower number of request>:<higher number of request>:<step>
```

Пример запуска:

```
perf_analyzer -m ru-toxicity-onnx -b 2 --shape input_ids:10 --shape token_type_ids:10 --shape attention_mask:10 --concurrency-range 2:4:2 --percentile=95
```

# Triton Model Analyzer

Model Analyzer - инструмент командной строки, анализирующий параметры конфигурации и генерирующий отчеты с кратким описанием производительности инференса модели в Triton. Таким образом, данный инструмент позволяет задать наилучшую конфигурацию модели, обобщив результаты о задержке, пропускной способности, использовании ресурсов графического процессора, энергопотреблении и многом другом с помощью генерации подробных отчетов, показателей и графиков.

Про возможные варианты установки 

Команда запуска контейнера для Model Analyzer можно почитать [здесь](https://github.com/triton-inference-server/model_analyzer/blob/main/docs/install.md#recommended-installation-method).

```
docker run -it --gpus all \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v <path-to-output-model-repo>:<path-to-output-model-repo> \
    --net=host nvcr.io/nvidia/tritonserver:${RELEASE}-py3-sdk
```


--triton-launch-mode=docker - автоматически запускается Triton сервер

tritonserver --model-repository=/models --model-control-mode=explicit !!!

model-analyzer profile --profile-models ru-toxicity-onnx --triton-launch-mode=remote --output-model-repository-path /model_analyzer/configs  --override-output-model-repository --latency-budget 10 --run-config-search-mode quick --client-protocol grpc -f perf.yaml -s /model_analyzer/checkpoints -e /model_analyzer


model-analyzer report --report-model-configs ru-toxicity-onnx_config_7 -e /model_analyzer


--backend-config=onnxruntime,default-max-batch-size=<int>