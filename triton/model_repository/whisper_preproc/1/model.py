import json

import numpy as np
import torch
import torchaudio
import triton_python_backend_utils as pb_utils
from whisper import log_mel_spectrogram


class TritonPythonModel:
    """Your Python model must use the same class name. Every Python model
    that is created must have "TritonPythonModel" as the class name.
    """

    def initialize(self, args):
        """`initialize` is called only once when the model is being loaded.
        Implementing `initialize` function is optional. This function allows
        the model to initialize any state associated with this model.

        Parameters
        ----------
        args : dict
          Both keys and values are strings. The dictionary keys and values are:
          * model_config: A JSON string containing the model configuration
          * model_instance_kind: A string containing model instance kind
          * model_instance_device_id: A string containing model instance device ID
          * model_repository: Model repository path
          * model_version: Model version
          * model_name: Model name
        """
        self.model_config = json.loads(args["model_config"])
        self.type_outputs = {
            x["name"]: pb_utils.triton_string_to_numpy(x["data_type"])
            for x in self.model_config["output"]
        }

    def get_log_mel(self, audiofile):
        max_samples = 30 * 16_000
        samples, sample_rate = torchaudio.load(audiofile)
        if sample_rate != 16_000:
            samples = torchaudio.transforms.Resample(
                orig_freq=sample_rate, new_freq=16_000
            )(samples)
        if samples.shape[1] < max_samples:
            silence = torch.zeros(max_samples - samples.shape[1]).unsqueeze(dim=0)
        samples = torch.concat([samples, silence], dim=1)
        samples_mel = log_mel_spectrogram(samples)
        return samples_mel

    def execute(self, requests):
        """`execute` MUST be implemented in every Python model. `execute`
        function receives a list of pb_utils.InferenceRequest as the only
        argument. This function is called when an inference request is made
        for this model. Depending on the batching configuration (e.g. Dynamic
        Batching) used, `requests` may contain multiple requests. Every
        Python model, must create one pb_utils.InferenceResponse for every
        pb_utils.InferenceRequest in `requests`. If there is an error, you can
        set the error argument when creating a pb_utils.InferenceResponse

        Parameters
        ----------
        requests : list
          A list of pb_utils.InferenceRequest

        Returns
        -------
        list
          A list of pb_utils.InferenceResponse. The length of this list must
          be the same as `requests`
        """

        # output0_dtype = self.output0_dtype

        responses = []

        print(requests)
        print("DONE0")
        for request in requests:
            # Get INPUT0

            filepath = pb_utils.get_input_tensor_by_name(
                request, "audio_path"
            ).as_numpy()[0][0]
            print(filepath)
            print("DONE!1")
            filepath = filepath.decode()
            print("DONE!2")

            x_mel = self.get_log_mel(filepath)
            print(x_mel)

            tensor_output = [
                pb_utils.Tensor(
                    "x_mel", np.array(x_mel).astype(self.type_outputs["x_mel"])
                )
            ]
            responses.append(pb_utils.InferenceResponse(tensor_output))

        # You should return a list of pb_utils.InferenceResponse. Length
        # of this list must match the length of `requests` list.
        return responses

    def finalize(self):
        """`finalize` is called only once when the model is being unloaded.
        Implementing `finalize` function is OPTIONAL. This function allows
        the model to perform any necessary clean ups before exit.
        """
        print("Cleaning up...")
