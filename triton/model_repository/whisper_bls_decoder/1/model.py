import json

import numpy as np
import triton_python_backend_utils as pb_utils
import whisper


class TritonPythonModel:
    def initialize(self, args):
        self.model_config = json.loads(args["model_config"])
        self.tokenizer = whisper.decoding.get_tokenizer(
            multilingual=False,
            task="transcribe",
            language="en",
        )
        self.max_tokens = 448
        self.model_config = json.loads(args["model_config"])
        self.type_outputs = {
            x["name"]: pb_utils.triton_string_to_numpy(x["data_type"])
            for x in self.model_config["output"]
        }

    def execute(self, requests):
        responses = []
        for request in requests:
            encoder_out = pb_utils.get_input_tensor_by_name(
                request, "encoder_out"
            ).as_numpy()
            encoder_out = encoder_out[np.newaxis, ...]
            print("BLS-encoder_out", encoder_out)
            x_tokens = list(self.tokenizer.sot_sequence_including_notimestamps)
            next_token = self.tokenizer.sot
            while len(x_tokens) <= self.max_tokens and next_token != self.tokenizer.eot:
                # Create inference request object
                print("DONE", encoder_out.shape)
                x_tokens_array = np.asarray(x_tokens, dtype="int64")
                x_tokens_array = x_tokens_array[np.newaxis, ...]
                print(x_tokens_array.shape)
                print(encoder_out.shape)
                inputs = [
                    pb_utils.Tensor("encoder_out", encoder_out),
                    pb_utils.Tensor("tokens", x_tokens_array),
                ]
                infer_request = pb_utils.InferenceRequest(
                    model_name="whisper_decoder_onnx",
                    requested_output_names=["decoder_out"],
                    inputs=inputs,
                    preferred_memory=pb_utils.PreferredMemory(
                        pb_utils.TRITONSERVER_MEMORY_CPU, 0)
                    # pb_utils.TRITONSERVER_MEMORY_GPU
                )
                # Perform synchronous blocking inference request
                infer_response = infer_request.exec()

                # Make sure that the inference response doesn't have an error. If
                # it has an error and you can't proceed with your model execution
                # you can raise an exception.
                if infer_response.has_error():
                    raise pb_utils.TritonModelException(
                        infer_response.error().message()
                    )

                decoder_out = pb_utils.get_output_tensor_by_name(
                    infer_response, "decoder_out"
                ).as_numpy()
                next_token = decoder_out[0, -1].argmax()
                x_tokens.append(next_token)

            text_output = self.tokenizer.decode(x_tokens)
            for x in self.tokenizer.special_tokens:
                text_output = text_output.replace(x, "")
            text_output = np.array([text_output]).astype(self.type_outputs["text"])
            text_output = text_output[np.newaxis, ...]
            tensor_output = [
                pb_utils.Tensor(
                    "text", text_output
                )
            ]
            responses.append(pb_utils.InferenceResponse(tensor_output))
        return responses

    def finalize(self):
        """`finalize` is called only once when the model is being unloaded.
        Implementing `finalize` function is OPTIONAL. This function allows
        the model to perform any necessary clean ups before exit.
        """
        print("Cleaning up...")
