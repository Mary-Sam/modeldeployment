import argparse
import os
from pathlib import Path

import torch
from onnxruntime.quantization import QuantType, quantize_dynamic
from transformers import AutoModelForSequenceClassification, AutoTokenizer


def convert_from_torch_to_onnx(
    onnx_path: str, tokenizer: AutoTokenizer, model: AutoModelForSequenceClassification
) -> None:
    """Конвертация модели из формата PyTorch в формат ONNX.

    onnx_path: путь к модели в формате ONNX
    tokenizer: токенизатор
    model: модель
    """
    dummy_model_input = tuple(
        x.to(model.device)
        for x in tokenizer(
            "You're a fucking nerd.",
            padding=True,
            truncation=True,
            return_tensors="pt",
        ).values()
    )
    torch.onnx.export(
        model,
        dummy_model_input,
        onnx_path,
        opset_version=12,
        do_constant_folding=True,
        input_names=["input_ids", "token_type_ids", "attention_mask"],
        output_names=["logits"],
        dynamic_axes={
            "input_ids": {0: "batch_size", 1: "sequence"},
            "token_type_ids": {0: "batch_size", 1: "sequence"},
            "attention_mask": {0: "batch_size", 1: "sequence"},
            "logits": {0: "batch_size"},
        },
    )


def convert_from_onnx_to_quantized_onnx(
    onnx_model_path: str,
    quantized_onnx_model_path: str,
    weight_type: QuantType = QuantType.QUInt8,
) -> None:
    """Квантизация модели в формате ONNX до Int8
    и сохранение кванитизированной модели на диск.

    onnx_model_path: путь к модели в формате ONNX
    quantized_onnx_model_path: путь к квантизированной модели
    weight_type: квантизация модели к данному типу
    """
    quantize_dynamic(
        onnx_model_path, quantized_onnx_model_path, weight_type=weight_type
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model_id",
        type=str,
        default="JungleLee/bert-toxic-comment-classification",
        help="Model id from hugging face",
    )
    parser.add_argument(
        "-op",
        "--onnx_path",
        type=str,
        default="triton/model_repository/en_toxicity_onnx/1/model.onnx",
        help="Path to save onnx model",
    )
    parser.add_argument(
        "-q",
        "--quantized",
        type=bool,
        default=False,
        help="Quantization for onnx model (True or False)",
    )
    parser.add_argument(
        "-oqp",
        "--onnx_quantized_path",
        type=str,
        default="triton/model_repository/en-toxicity-onnx-quantized/1/model.onnx",
        help="Path to save onnx model with quantized",
    )
    parser.add_argument(
        "-wt",
        "--weight_type",
        type=QuantType,
        default=QuantType.QUInt8,
        help="The type to which the model will be quantized",
    )
    args = parser.parse_args()
    tokenizer = AutoTokenizer.from_pretrained(args.model_id)
    model = AutoModelForSequenceClassification.from_pretrained(args.model_id)
    if torch.cuda.is_available():
        model.cuda()

    if not os.path.exists(args.onnx_path):
        os.makedirs(Path(args.onnx_path).parent, exist_ok=True)
        convert_from_torch_to_onnx(args.onnx_path, tokenizer, model)
        if args.quantized:
            os.makedirs(Path(args.onnx_quantized_path).parent, exist_ok=True)
            convert_from_onnx_to_quantized_onnx(
                args.onnx_path, args.onnx_quantized_path, args.weight_type
            )
