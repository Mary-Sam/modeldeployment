import argparse

import numpy as np
from onnxruntime import InferenceSession
from transformers import AutoTokenizer

from utils.onnx_session import create_onnx_session


def softmax(_outputs):
    maxes = np.max(_outputs, axis=-1, keepdims=True)
    shifted_exp = np.exp(_outputs - maxes)
    return shifted_exp / shifted_exp.sum(axis=-1, keepdims=True)


def onnx_inference(
    text: str,
    session: InferenceSession,
    tokenizer: AutoTokenizer,
) -> np.ndarray:
    """Инференс модели с помощью ONNX Runtime.

    @param text: входной текст для классификации
    @param session: ONNX Runtime-сессия
    @param tokenizer: токенизатор
    @param max_length: максимальная длина последовательности в токенах
    @return: логиты на выходе из модели
    """
    inputs = tokenizer(
        text,
        padding=True,
        truncation=True,
        return_tensors="pt",
    )
    input_feed = {
        "input_ids": inputs["input_ids"].numpy(),
        "token_type_ids": inputs["token_type_ids"].numpy(),
        "attention_mask": inputs["attention_mask"].numpy(),
    }
    outputs = session.run(output_names=["logits"], input_feed=input_feed)
    return outputs


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-t",
        "--text",
        type=str,
        help="English text for toxicity classification.",
    )
    parser.add_argument(
        "-tz",
        "--tokenizer",
        type=str,
        default="JungleLee/bert-toxic-comment-classification",
        help="Model id for tokenizer: default='JungleLee/bert-toxic-comment-classification'",
    )
    parser.add_argument(
        "-m",
        "--model_path",
        type=str,
        default="triton/model_repository/en-toxicity-onnx/1/model.onnx",
        help="Path to onnx model",
    )
    args = parser.parse_args()
    tokenizer = AutoTokenizer.from_pretrained('triton/model_repository/toxicity_preproc/1')
    session = create_onnx_session(args.model_path)
    predict = onnx_inference(args.text, session, tokenizer)
    scores = softmax(predict[0])
    id2label = {0: "non-toxic", 1: "toxic"}
    res = {"label": id2label[scores.argmax().item()], "score": scores.max().item()}
    print(f"Text: <{args.text}> is {res['label']} with probability {res['score']}")
