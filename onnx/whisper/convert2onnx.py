import argparse

import torch
import whisper

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-m",
        "--model_id",
        type=str,
        default="tiny.en",
        help="Size model",
    )
    parser.add_argument(
        "-e",
        "--onnx_encoder_path",
        type=str,
        default="triton/model_repository/whisper_encoder_onnx/1/model.onnx",
        help="Path to save onnx encoder model",
    )
    parser.add_argument(
        "-d",
        "--onnx_decoder_path",
        type=str,
        default="triton/model_repository/whisper_decoder_onnx/1/model.onnx",
        help="Path to save onnx decoder model",
    )
    parser.add_argument(
        "-mt",
        "--max_tokens",
        type=int,
        default=448,
        help="Path to save onnx decoder model",
    )

    args = parser.parse_args()
    model = whisper.load_model(args.model_id)
    model.requires_grad_(False)
    model.eval()

    tokenizer = whisper.decoding.get_tokenizer(
        multilingual=False,
        task="transcribe",
        language="en",
    )

    # Модель whisper работает с аудио длиной 30 сек
    # На вход подается мел-частотные кепстральный коэффициенты
    # x_mel shape: [batch, coeff=80, time=3000]
    x_mel = torch.randn(1, 80, 3000).to(model.device)

    x_audio = model.encoder(x_mel)

    # Текст декодируется с помощью авторегрессии в декодере
    # shape: [batch, seq<=448]
    x_tokens = torch.tensor(
        [tokenizer.sot_sequence_including_notimestamps],
        dtype=torch.long,
    ).to(model.device)

    # декодер в цикле предсказывает следующий токен до тех пор, пока
    # не будет предсказан токен конца текста или максимальное кол-во токенов
    next_token = tokenizer.sot
    while x_tokens.shape[1] <= args.max_tokens and next_token != tokenizer.eot:
        y_tokens = model.decoder(x_tokens, x_audio)
        next_token = y_tokens[0, -1].argmax()
        x_tokens = torch.concat(
            [x_tokens, next_token.reshape(1, 1)],
            axis=1,
        )

    #: ONNX export
    torch.onnx.export(
        model.encoder,
        (x_mel,),
        args.onnx_encoder_path,
        input_names=["x_mel"],
        output_names=["encoder_out"],
        dynamic_axes={
            "x_mel": {0: "batch"},
            "encoder_out": {0: "batch"},
        },
    )

    torch.onnx.export(
        model.decoder,
        (x_tokens, x_audio),
        args.onnx_decoder_path,
        input_names=["tokens", "encoder_out"],
        output_names=["decoder_out"],
        dynamic_axes={
            "tokens": {0: "batch", 1: "seq"},
            "encoder_out": {0: "batch"},
            "decoder_out": {0: "batch", 1: "seq"},
        },
    )
