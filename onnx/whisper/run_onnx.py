import argparse

import numpy as np
import whisper

from utils.onnx_session import create_onnx_session
from utils.processing_data import get_log_mel

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input_file",
        type=str,
        default="data/en_Alex_English_0_24000.wav",
        help="Input file with speech",
    )
    parser.add_argument(
        "-e",
        "--onnx_encoder_path",
        type=str,
        default="triton/model_repository/whisper_encoder_onnx/1/model.onnx",
        help="Path to save onnx encoder model",
    )
    parser.add_argument(
        "-d",
        "--onnx_decoder_path",
        type=str,
        default="triton/model_repository/whisper_decoder_onnx/1/model.onnx",
        help="Path to save onnx decoder model",
    )
    parser.add_argument(
        "-mt",
        "--max_tokens",
        type=int,
        default=448,
        help="Path to save onnx decoder model",
    )
    args = parser.parse_args()
    mel_from_audio = get_log_mel(args.input_file)
    sess_encoder = create_onnx_session(args.onnx_encoder_path)
    sess_decoder = create_onnx_session(args.onnx_decoder_path)

    (encoder_out,) = sess_encoder.run(
        ["encoder_out"], {"x_mel": mel_from_audio.numpy()}
    )

    tokenizer = whisper.decoding.get_tokenizer(
        multilingual=False,
        task="transcribe",
        language="en",
    )
    x_tokens = list(tokenizer.sot_sequence_including_notimestamps)
    # tokens = []

    next_token = tokenizer.sot
    while len(x_tokens) <= args.max_tokens and next_token != tokenizer.eot:
        (decoder_out,) = sess_decoder.run(
            ["decoder_out"],
            {
                "tokens": np.asarray([x_tokens], dtype="int64"),
                "encoder_out": encoder_out,
            },
        )
        next_token = decoder_out[0, -1].argmax()
        x_tokens.append(next_token)

    text = tokenizer.decode(x_tokens)
    for x in tokenizer.special_tokens:
        text = text.replace(x, "")
    print(text)
